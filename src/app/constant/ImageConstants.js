import CommmunityImage from "../../assets/img/Community-img.png";
import CommmunityImage_1 from "../../assets/img/Community-img1.png";
import CommmunityImage_2 from "../../assets/img/Community-img2.png";
import CommmunityImage_3 from "../../assets/img/Community-img3.png";
import CommmunityImage_4 from "../../assets/img/Community-img4.png";
import CommmunityImage_5 from "../../assets/img/Community-img5.png";
import CommmunityImage_Layer from "../../assets/img/CompositeLayer .png";
import BannerContact from "../../assets/img/Image_Contact.png";
import MicrosoftImage from "../../assets/img/Microsoft.png";
import Nintendo from "../../assets/img/Nintendo.png";
import Steam from "../../assets/img/steam.png";
import ExcluCard from "../../assets/img/Exclu.png";
import SpecialCard from "../../assets/img/SpecialCard.png";
import LayerSlider from "../../assets/img/layer-slider.png";

import { ReactComponent as LinkedMobile } from "../../assets/icons/linkedinMobile.svg";
import { ReactComponent as TwitterMobile } from "../../assets/icons/twitterMobile.svg";
import { ReactComponent as YoutubeMobile } from "../../assets/icons/youtubeMobile.svg";
import { ReactComponent as FacebookMobile } from "../../assets/icons/facebookMobile.svg";
import { ReactComponent as FaceBook } from "../../assets/icons/facebook.svg";
import { ReactComponent as In } from "../../assets/icons/in.svg";
import { ReactComponent as Twitter } from "../../assets/icons/twiter.svg";
import { ReactComponent as Youtube } from "../../assets/icons/youtube.svg";
import { ReactComponent as Instagram } from "../../assets/icons/IconCommnutity/iconsIg.svg";
import { ReactComponent as FacebookCommunity } from "../../assets/icons/IconCommnutity/facebook.svg";
import { ReactComponent as TwitterCommunity } from "../../assets/icons/IconCommnutity/twitter.svg";

export const ICONS_CONSTANTS = {
  LinkedMobile,
  YoutubeMobile,
  TwitterMobile,
  FacebookMobile,
  FaceBook,
  In,
  Twitter,
  Youtube,
  Instagram,
  FacebookCommunity,
  TwitterCommunity,
};

export const IMAGE_CONSTANTS = {
  CommmunityImage,
  CommmunityImage_1,
  CommmunityImage_2,
  CommmunityImage_3,
  CommmunityImage_4,
  CommmunityImage_5,
  CommmunityImage_Layer,
  BannerContact,
  MicrosoftImage,
  Nintendo,
  Steam,
  ExcluCard,
  SpecialCard,
  LayerSlider,
};
