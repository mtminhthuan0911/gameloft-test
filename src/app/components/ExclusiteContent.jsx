import React from "react";
import styled from "styled-components";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/effect-coverflow";
import { Navigation, Pagination, FreeMode } from "swiper";
import KeyboardBackspaceIcon from "@material-ui/icons/KeyboardBackspace";
import { ExclusiveData } from "../data/DataMumy";
import { IMAGE_CONSTANTS } from "../constant/ImageConstants";
import VerticalAlignBottomIcon from "@material-ui/icons/VerticalAlignBottom";

const ExclusiteStyled = styled.div`
  && {
    &.exclusive {
      width: 100%;
      height: calc(var(--vh) * 100 - 85);
      .exclusive {
        &-container {
          max-width: 1020px;
          margin: 0 auto;
          .header {
            padding-top: 196px;
            color: #2699fb;
            .title {
              font-weight: bold;
              font-size: 45px;
              padding-bottom: 20px;
            }
            .subTitle {
              font-size: 25px;
              color: #2699fb;
              font-weight: 400;
            }
          }
        }
        @media (max-width: 768px) {
          &-container {
            padding: 0 21px;
            .header {
              padding-top: 56px;
              .title {
                font-size: 25px;
              }
              .subTitle {
                font-size: 20px;
              }
            }
          }
        }
        &-slider {
          margin: 85px 0;
          .slider-exclusive {
            .slider-container {
              img {
                position: relative;
              }
              .card-info {
                position: absolute;
                top: 25px;
                right: 0;
                .card-info-top {
                  background-color: #fff;
                  width: 40px;
                  height: 40px;
                  border-radius: 100%;
                  position: relative;
                  svg {
                    position: absolute;
                    top: 6px;
                    left: 7px;
                    path {
                      fill: #000;
                    }
                  }
                  &:hover {
                    svg {
                      path {
                        fill: #2699fb;
                      }
                    }
                  }
                }
              }
            }
          }
          .slider-controler {
            width: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
            margin: 10px 0;
            color: #2699fb;
            margin: 85px 0;
            .slide-next-btnExclusive {
              cursor: pointer;
              svg {
                font-size: 50px;
                transform: rotate(180deg);
              }
            }
            .pagination-exclusive {
              font-size: 20px;
              padding: 0 20px;
              .swiper-pagination-current {
                font-weight: bold;
                position: relative;
                bottom: 5px;
              }
            }
            .slide-prev-btnExclusive {
              cursor: pointer;
              svg {
                font-size: 50px;
              }
            }
          }
        }
        @media (max-width: 768px) {
          &-slider {
            margin: 20px 0;
            .slider-exclusive {
              .swiper-slide {
              }
              .slider-container {
                width: 100%;
                max-width: 100%;
                .card-info {
                  right: 35px;
                }
                img {
                  width: 100%;
                  max-width: 100%;
                }
              }
            }
            .slider-controler {
              margin: 29px 0;
            }
          }
        }
      }
    }
  }
`;

function ExclusiteContent() {
  const isMobileDetect = window.innerWidth < 768;

  return (
    <ExclusiteStyled className="exclusive">
      <div className="exclusive-container">
        <div className="header">
          <div className="title">Exclusive Game Content</div>
          <div className="subTitle">
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
            nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
            erat, sed diam voluptua. At vero eos.
          </div>
        </div>
      </div>
      <div className="exclusive-slider">
        <Swiper
          slidesPerView={isMobileDetect ? 1 : 3}
          spaceBetween={isMobileDetect ? 0 : 100}
          freeMode={false}
          loop={true}
          centeredSlides={isMobileDetect ? true : false}
          modules={[Navigation, Pagination, FreeMode]}
          pagination={{
            el: ".pagination-exclusive",
            type: "fraction",
          }}
          navigation={{
            nextEl: ".slide-next-btnExclusive",
            prevEl: ".slide-prev-btnExclusive",
          }}
          className="slider-exclusive"
        >
          {ExclusiveData.map((item, index) => (
            <SwiperSlide>
              <div className="slider-container">
                <img
                  src={IMAGE_CONSTANTS.ExcluCard}
                  alt="img"
                  className={`thumbnail slide-${item.id}`}
                />
                <div className="card-info">
                  <div className="card-info-top">
                    <a href={IMAGE_CONSTANTS.ExcluCard} download>
                      <div className="download">
                        <VerticalAlignBottomIcon />
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            </SwiperSlide>
          ))}
        </Swiper>
        <div className="slider-controler">
          <div className="slide-prev-btnExclusive">
            <KeyboardBackspaceIcon />
          </div>
          <div className="pagination-exclusive">
            <span className="paging-left">1</span>/10
          </div>
          <div className="slide-next-btnExclusive">
            <KeyboardBackspaceIcon />
          </div>
        </div>
      </div>
    </ExclusiteStyled>
  );
}

export default ExclusiteContent;
