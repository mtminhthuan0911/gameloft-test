import React from "react";
import styled from "styled-components";
import BannerImage from "../../assets/img/Banner.png";
import Rating from "@mui/material/Rating";
import IconShare from "@material-ui/icons/ShareOutlined";
import IconContact from "@material-ui/icons/HeadsetMicOutlined";
import IconMail from "@material-ui/icons/MailOutlineOutlined";

const descriptionProductDesktop =
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud";
const desciptionProductMobile =
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua";

const BannerStyled = styled.div`
  cursor: pointer;
  position: relative;
  min-height: calc(var(--vh) * 100);
  &:before {
    content: "";
    top: 0;
    left: 0;
    position: absolute;
    width: 100%;
    height: 100%;
    z-index: 1;
  }
  img {
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    object-fit: cover;
    max-width: 100%;
    max-height: 100%;
    object-position: top;
    width: 100%;
    height: 100%;
  }
  .product-container {
    position: absolute;
    bottom: 50px;
    width: 100%;
    .product-container-block {
      max-width: 1020px;
      margin: 0 auto;
      .product-banner-info {
        text-align: left;
        color: #fff;
        max-width: 600px;
        .product-title {
          font-size: 45px;
          font-weight: bold;
          line-height: 45px;
        }
        .product-subTitle-rating {
          display: flex;
          align-items: center;
          font-size: 11px;
          margin: 10px 0 40px;
          .chart {
            padding: 0 5px;
          }
        }
        .product-description {
          color: #fff;
          font-size: 20px;
        }
      }
    }
  }
  .product-banner-icon-social {
    display: flex;
    align-items: flex-end;
    position: fixed;
    bottom: 50px;
    right: 80px;
    z-index: 9999;
    .block-container {
      display: flex;
      flex-direction: column;
      .btn {
        color: rgb(0, 0, 0);
        width: 33px;
        height: 33px;
        border-radius: 100px;
        text-align: center;
        background: rgb(255, 255, 255);
        margin-top: 8px;
        padding: 4px;
        display: flex;
        align-items: center;
        justify-content: center;
        transition: all 0.3s;
        position: relative;
        z-index: 2;
        &:hover {
          color: #2699fb;
          &:before {
            content: "";
            position: absolute;
            width: 53px;
            height: 53px;
            border-radius: 100px;
            background: rgb(255, 255, 255);
            opacity: 0.7;
            z-index: -1;
          }
        }
      }
    }
  }
  @media (max-width: 768px) {
    min-height: calc(var(--vh) * 100);
    .product-container {
      .product-container-block {
        .product-banner-info {
          padding-left: 20px;
          max-width: 277px;
          .product-description {
            font-size: 12px;
          }
        }
      }
    }
    .product-banner-icon-social {
      bottom: 50px;
      right: 21px;
      .block-container {
        .btn {
          width: 24px;
          height: 24px;
          svg {
            font-size: 18px;
          }
          &:hover {
            &:before {
              width: 33px;
              height: 33px;
            }
          }
        }
      }
    }
  }
`;

function Banner() {
  const isMobileDetect = window.innerWidth < 768;
  return (
    <BannerStyled className="product-banner">
      <img src={BannerImage} className="product-banner-img" alt="Banner" />
      <div className="product-container">
        <div className="product-container-block">
          <div className="product-banner-info">
            <div className="product-title">Gameloft game</div>
            <div className="product-subTitle-rating">
              <span className="subTitle">Racing / Action</span>{" "}
              <span className="chart">|</span>
              <Rating name="size-small" value={4} size="small" readOnly />
            </div>
            <div className="product-description">
              {isMobileDetect
                ? desciptionProductMobile
                : descriptionProductDesktop}
            </div>
          </div>
        </div>
      </div>
      <div className="product-banner-icon-social">
        <div className="block-container">
          <div className="btn share">
            <IconShare />
          </div>
          <div className="btn mail">
            <IconMail />
          </div>
          <div className="btn contact">
            <IconContact />
          </div>
        </div>
      </div>
    </BannerStyled>
  );
}

export default Banner;
