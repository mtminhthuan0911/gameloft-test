import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/effect-coverflow";
import { Navigation, Pagination, EffectCoverflow, FreeMode } from "swiper";
import { gameCommunityData, socialDataCommunity } from "../data/DataMumy";
import IconButton from "@material-ui/core/IconButton";
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";
import { TextField } from "@material-ui/core";
import KeyboardBackspaceIcon from "@material-ui/icons/KeyboardBackspace";
import ShareIcon from "@material-ui/icons/Share";
import { IMAGE_CONSTANTS } from "../constant/ImageConstants";
import _ from "lodash";

const CommnunityStyled = styled.div`
  && {
    &.community {
      width: 100%;
      height: calc(var(--vh) * 100 - 80);
      background-color: #fff;
      color: #2699fb;
      .community {
        &-container {
          .community-header {
            width: 100%;
            max-width: 1020px;
            margin: 0 auto;
            padding-top: 59px;
            p {
              margin: 0;
            }
            .title {
              font-size: 45px;
              font-weight: bold;
              line-height: 45px;
            }
            .subTitle {
              font-size: 25px;
              font-weight: bold;
              line-height: 25px;
              padding-top: 31px;
            }
            .slider {
              margin-top: 50px;
              .mySwiper {
                .swiper-button-prev,
                .swiper-button-next {
                  transform: translateY(-21px);
                }
                .item-slide {
                  text-align: center;
                  transform: scale(0.9);
                  .item-container {
                    img {
                      width: 100%;
                      height: 100%;
                      max-width: 90px;
                      max-height: 90px;
                      min-height: 90px;
                      object-fit: cover;
                      border-radius: 100%;
                      &.slide-0 {
                        border: 3px solid #2699fb;
                      }
                    }
                    .short-title {
                      padding-top: 21px;
                    }
                  }
                }
              }
            }
          }
          .community-post {
            width: 100%;
            margin-top: 60px;
            .header {
              max-width: 1020px;
              margin: 0 auto;
              display: flex;
              align-items: center;
              justify-content: space-between;
              .title {
                font-size: 30px;
                font-weight: bold;
                line-height: 30px;
              }
              .icons-social {
                margin-left: auto;
                cursor: pointer;
                a {
                  margin-left: 24px;
                }
              }
              .action {
                display: flex;
                align-items: center;
                .search-product {
                  margin-left: 24px;
                  margin-top: 7px;
                  .search-input {
                    div {
                      color: #2699fb;
                    }
                    svg {
                      color: #2699fb;
                    }
                    input {
                      color: #2699fb;
                      padding: 10px 12px;
                    }
                    input::placeholder {
                      color: #2699fb;
                      opacity: 1;
                      font-style: italic;
                    }
                    fieldset {
                      border-color: #2699fb;
                    }
                    label {
                      color: #2699fb;
                    }
                  }
                }
              }
            }
            .slider-community {
              margin-top: 30px;
              .overlay-mobile {
                display: none;
              }
              .slider-container {
                position: relative;
                display: flex;
                justify-content: center;
                .card-info {
                  display: flex;
                  flex-direction: column;
                  position: absolute;
                  width: 100%;
                  height: 100%;
                  padding: 20px 18px;
                  top: 0;
                  max-width: 337px;
                  .card-info-top {
                    display: flex;
                    .share {
                      margin-left: auto;
                      transition: all 0.3s;
                      position: relative;
                      z-index: 2;
                      color: #000;
                      background: #fff;
                      width: 40px;
                      height: 40px;
                      border-radius: 100%;
                      svg {
                        position: absolute;
                        left: 6px;
                        bottom: 9px;
                      }
                      &:hover {
                        color: #2699fb;
                      }
                    }
                  }
                  .card-info-bottom {
                    margin-top: auto;
                  }
                }
              }
            }
            .slider-controler {
              width: 100%;
              display: flex;
              justify-content: center;
              align-items: center;
              margin: 10px 0;
              .slide-next {
                cursor: pointer;
                svg {
                  font-size: 50px;
                  transform: rotate(180deg);
                }
              }
              .pagination {
                font-size: 20px;
                padding: 0 20px;
                .swiper-pagination-current {
                  font-weight: bold;
                  position: relative;
                  bottom: 5px;
                }
              }
              .slide-prev {
                cursor: pointer;
                svg {
                  font-size: 50px;
                }
              }
            }
          }
        }
      }
      @media (max-width: 768px) {
        padding: 0 21px;
        .community {
          &-container {
            .community-header {
              padding-top: 32px;
              .title {
                font-size: 25px;
              }
              .subTitle {
                display: none;
              }
              .slider {
                margin-top: 8px;
                .mySwiper {
                  .item-slide {
                    transform: scale(0.8);
                  }
                }
              }
            }
            .community-post {
              margin-top: 0;
              .header {
                display: block;
                .title {
                  font-size: 20px;
                }
                .action {
                  display: block;
                  .search-product {
                    margin: 0;
                    .search-input {
                      width: 100%;
                    }
                  }
                }
                .postMobile {
                  display: flex;
                  margin-bottom: 12px;
                  align-items: center;
                }
              }
              .slider-community {
                max-height: calc(var(--vh) * 100);
                height: calc(var(--vh) * 100 - 57);
                width: 100%;
                min-width: 100%;
                .overlay-mobile {
                  display: block;
                  img {
                    max-width: 100%;
                    max-height: 100%;
                    position: absolute;
                    z-index: 999;
                    bottom: 0px;
                  }
                }
                .slider-container {
                  width: 100%;
                  max-width: 100%;
                }
                .swiper-slide {
                  width: 100%;
                  padding-bottom: 35vh;
                  max-height: calc(var(--vh) * 100 - 57);
                  img {
                    width: 100%;
                  }
                }
              }
              .btn {
                text-align: center;
                display: flex;
                justify-content: center;
                &.more {
                  margin: 10px 0px;
                  padding: 10px 60px;
                  border: 1px solid #2699fb;
                  color: #2699fb;
                  font-size: 15px;
                  &:hover {
                    color: #fff;
                    background-color: #2699fb;
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`;

function Community(props) {
  const isMobileDetect = window.innerWidth < 768;
  const [arrContentCommunity, setArrContenttCommunity] =
    useState(gameCommunityData);
  const [searchTerm, setSearchTerm] = useState();

  const onChangeText = (key, e) => {
    let value = "";
    value = e.target.value;
    setSearchTerm(value);
  };

  function getSearchTerm(searchTerm) {
    if (_.isEmpty(searchTerm)) {
      setArrContenttCommunity(gameCommunityData);
      return;
    }
    let newArrResult = _.filter(gameCommunityData, (item) => {
      return (
        item.category.match(searchTerm) || item.description.match(searchTerm)
      );
    });
    setArrContenttCommunity(newArrResult);
  }

  useEffect(() => {
    const debounceSearch = _.debounce(() => {
      getSearchTerm(searchTerm);
    }, 1000);
    debounceSearch();
  }, [searchTerm]);

  return (
    <CommnunityStyled className="community">
      <div className="community-container">
        <div className="community-header">
          <p className="title">Game Community Hub</p>
          <p className="subTitle">Live Game Updates</p>
          <div className="slider">
            <Swiper
              navigation={true}
              modules={[Navigation]}
              slidesPerView={isMobileDetect ? 3 : 6}
              spaceBetween={0}
              className="mySwiper"
            >
              {gameCommunityData.map((item, index) => (
                <SwiperSlide className={`item-slide`}>
                  <div className={`item-container`}>
                    <a
                      href="https://mtminhthuan0911.gitlab.io/gameloft-test"
                      target="_blank"
                      rel="noreferrer"
                      className=""
                    >
                      <img
                        src={item.img}
                        alt="img"
                        className={`thumbnail slide-${item.id}`}
                      />
                    </a>
                    <p className="short-title">{item.shortTitle}</p>
                  </div>
                </SwiperSlide>
              ))}
            </Swiper>
          </div>
        </div>
        <div className="community-post">
          <div className="header">
            {isMobileDetect ? (
              <div className="postMobile">
                <div className="title">ALL Post</div>
                <div className="icons-social">
                  {socialDataCommunity.map((item) => {
                    return (
                      <a
                        href={item.link}
                        target="_blank"
                        className={`social ${item.id}`}
                        id={item.id}
                        rel="noreferrer"
                      >
                        {item.name}
                      </a>
                    );
                  })}
                </div>
              </div>
            ) : (
              <>
                <div className="title">ALL Post</div>
                <div className="icons-social">
                  {socialDataCommunity.map((item) => {
                    return (
                      <a
                        href={item.link}
                        rel="noreferrer"
                        target="_blank"
                        className={`social ${item.id}`}
                        id={item.id}
                      >
                        {item.name}
                      </a>
                    );
                  })}
                </div>
              </>
            )}
            <div className="action">
              <div className="search-product">
                <TextField
                  variant="outlined"
                  hiddenLabel
                  name="search"
                  placeholder="Search"
                  className="search-input"
                  onChange={(e) => onChangeText("search", e)}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment>
                        <IconButton>
                          <SearchIcon />
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </div>
            </div>
          </div>

          <Swiper
            effect={"coverflow"}
            grabCursor={true}
            centeredSlides={false}
            loop={true}
            spaceBetween={isMobileDetect ? 200 : 0}
            coverflowEffect={{
              rotate: 0,
              stretch: 0,
              depth: 0,
              modifier: isMobileDetect ? 0 : 0.5,
              slideShadows: 0,
            }}
            direction={isMobileDetect ? "vertical" : "horizontal"}
            slidesPerView={isMobileDetect ? 3 : 5}
            pagination={{
              el: ".pagination",
              type: "fraction",
            }}
            navigation={{
              nextEl: ".slide-next",
              prevEl: ".slide-prev ",
            }}
            modules={[EffectCoverflow, Navigation, Pagination, FreeMode]}
            className="slider-community"
          >
            {!_.isEmpty(arrContentCommunity) &&
              arrContentCommunity.map((item, index) => (
                <SwiperSlide>
                  <div className="slider-container">
                    <img
                      src={IMAGE_CONSTANTS.CommmunityImage_Layer}
                      alt="img"
                      className={`thumbnail slide-${item.id}`}
                    />
                    <div className="card-info">
                      <div className="card-info-top">
                        <div className="facebook">
                          {socialDataCommunity.map(
                            (icon) =>
                              icon.id === item.category && (
                                <span
                                  className={`social ${item.id}`}
                                  id={item.id}
                                >
                                  {icon.name}
                                </span>
                              )
                          )}
                        </div>
                        <div className="share">
                          <ShareIcon className="icon hoverIconAroud" />
                        </div>
                      </div>
                      <div className="card-info-bottom">{item.description}</div>
                    </div>
                  </div>
                </SwiperSlide>
              ))}
            <div className="overlay-mobile">
              <img src={IMAGE_CONSTANTS.LayerSlider} alt="img" />
            </div>
          </Swiper>
          {!isMobileDetect ? (
            <div className="slider-controler">
              <div className="slide-prev">
                <KeyboardBackspaceIcon />
              </div>
              <div className="pagination">
                <span className="paging-left">1</span>/10
              </div>
              <div className="slide-next">
                <KeyboardBackspaceIcon />
              </div>
            </div>
          ) : (
            <div className="btn">
              <button className="btn more">More</button>
            </div>
          )}
        </div>
      </div>
    </CommnunityStyled>
  );
}

export default Community;
