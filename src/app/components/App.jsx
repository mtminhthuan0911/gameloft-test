import React from "react";
import styled from "styled-components";
import "../main.scss";
import {
  Banner,
  Header,
  SpecialEvent,
  ExclusiteContent,
  Community,
  Footer,
  DownloadVersion,
  ContactRegister,
} from "./index";

const AppStyled = styled.div`
  && {
  }
`;

function App() {
  return (
    <AppStyled id="app" className="app">
      <Header />
      <Banner />
      <DownloadVersion />
      <ContactRegister />
      <Community />
      <SpecialEvent />
      <ExclusiteContent />
      <Footer />
    </AppStyled>
  );
}

export default App;
