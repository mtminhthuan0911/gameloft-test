import {
  TextField,
  MenuItem,
  FormGroup,
  FormControlLabel,
  Checkbox,
  Button,
  Select,
} from "@material-ui/core";
import React from "react";
import styled from "styled-components";
import { IMAGE_CONSTANTS } from "../constant/ImageConstants";

const ContactRegisterStyled = styled.div`
  && {
    &.contact-register {
      width: 100%;
      position: relative;
      height: calc(var(--vh) * 100 - 80px);
      &:before {
        content: "";
        position: absolute;
        width: 100%;
        height: 100%;
        background-color: #d8edff;
        opacity: 0.7;
        z-index: -1;
      }
      .contact-register {
        &-container {
          max-width: 1020px;
          margin: 0px auto;
          height: 100%;
          display: flex;
          align-items: center;
          &-block {
            display: flex;
            justify-content: space-between;
            width: 100%;
            align-items: flex-start;
            .form-contact {
              color: #2699fb;
              width: 40%;
              p {
                margin: 0;
              }
              &-title {
                font-size: 25px;
                font-weight: bold;
                text-align: left;
                line-height: 25px;
              }
              &-subTitle {
                font-weight: 500;
                font-size: 12px;
                padding-top: 5px;
              }
              .form {
                padding-top: 27px;
                .txt-field {
                  display: block;
                  margin-bottom: 19px;
                  .MuiSelect-root {
                    padding: 10.5px 10px;
                  }
                  div {
                    color: #2699fb;
                  }
                  svg {
                    color: #2699fb;
                  }
                  input {
                    color: #2699fb;
                  }
                  input::placeholder {
                    color: #2699fb;
                    opacity: 1;
                  }
                  fieldset {
                    border-color: #2699fb;
                  }
                  label {
                    color: #2699fb;
                  }
                }
                .txt-checkbox {
                  margin-top: 15px;
                  label {
                    align-items: flex-start;
                    color: #2699fb;
                  }
                  span {
                    padding: 2px 5px;
                    font-size: 10px;
                    font-weight: 400;
                  }
                  svg {
                    color: #2699fb;
                  }
                  .Mui-check {
                    color: #2699fb;
                  }
                }
                .btn {
                  background: none;
                  text-align: center;
                  width: 100%;
                  .confirm {
                    border: 1px solid #2699fb;
                    border-radius: 3px;
                    text-align: center;
                    padding: 11px 30px;
                    color: #2699fb;
                    margin-top: 10px;
                    font-size: 10px;
                    &:hover {
                      background-color: #2699fb;
                      color: #fff;
                    }
                  }
                }
              }
            }
          }
        }
      }
      @media (max-width: 768px) {
        &.contact-register {
          height: 100%;
          .contact-register {
            &-container {
              align-items: flex-start;
              padding: 81px 21px;
              &-block {
                display: block;
                .image-contact {
                  display: none;
                }
                .form-contact {
                  width: 100%;
                }
              }
            }
          }
        }
      }
    }
  }
`;

const countryData = [
  { value: "vn", name: "Viet Nam" },
  { value: "thai", name: "Thai Lan" },
  { value: "france", name: "France" },
];

const platformData = [
  { value: "mobile", name: "Mobile" },
  { value: "desktop", name: "Desktop" },
];

const ContactRegisterForm = () => {
  const [country, setCountry] = React.useState([]);
  const [platform, setFlatForm] = React.useState([]);
  const handleChangeCountry = (event) => {
    const {
      target: { value },
    } = event;
    setCountry(typeof value === "string" ? value.split(",") : value);
  };
  const handleChangePlatForm = (event) => {
    const {
      target: { value },
    } = event;
    setFlatForm(typeof value === "string" ? value.split(",") : value);
  };
  return (
    <div className="form">
      <TextField
        className="txt-field"
        placeholder="Name"
        id="filled-hidden-label-small"
        variant="outlined"
        size="small"
        fullWidth
      />
      <TextField
        className="txt-field"
        hiddenLabel
        id="filled-hidden-label-small"
        placeholder="Email"
        variant="outlined"
        size="small"
        fullWidth
      />
      <Select
        id="outlined-select-currency"
        select
        displayEmpty
        variant="outlined"
        size="small"
        value={country}
        onChange={handleChangeCountry}
        renderValue={(selected) => {
          if (selected.length === 0) {
            return <div>Country</div>;
          }

          return selected.join(", ");
        }}
        fullWidth
        className="txt-field"
      >
        <MenuItem disabled value="">
          <div>Country</div>
        </MenuItem>
        {countryData.map((option) => (
          <MenuItem key={option.value} value={option.name}>
            {option.name}
          </MenuItem>
        ))}
      </Select>
      <Select
        id="outlined-select-currency"
        value={platform}
        select
        variant="outlined"
        displayEmpty
        onChange={handleChangePlatForm}
        size="small"
        fullWidth
        className="txt-field"
        renderValue={(selected) => {
          if (selected.length === 0) {
            return <div>Platform</div>;
          }

          return selected.join(", ");
        }}
      >
        <MenuItem disabled value="">
          <div>Platform</div>
        </MenuItem>
        {platformData.map((option) => (
          <MenuItem key={option.value} value={option.name}>
            {option.name}
          </MenuItem>
        ))}
      </Select>
      <FormGroup className="txt-checkbox">
        <FormControlLabel
          control={<Checkbox />}
          label="By signing up, I confirm that I am 13 years old or older. I agree to the Gameloft Terms and Conditions and I have read the Privacy Policy."
        />
        <FormControlLabel
          control={<Checkbox />}
          label="I agree to receive promotional offers relating to all Gameloft games and services."
        />
      </FormGroup>
      <div className="btn">
        <Button className="confirm">Button</Button>
      </div>
    </div>
  );
};

function ContactRegister() {
  return (
    <ContactRegisterStyled className="contact-register">
      <div className="contact-register-container">
        <div className="contact-register-container-block">
          <div className="image-contact">
            <img src={IMAGE_CONSTANTS.BannerContact} alt="banner-contact" />
          </div>
          <div className="form-contact">
            <p className="form-contact-title">Stay in the Know!</p>
            <p className="form-contact-subTitle">
              Don't get left behind!{" "}
              <span style={{ display: "block" }}>
                Subscribe to our newsletters today!
              </span>
              <ContactRegisterForm />
            </p>
          </div>
        </div>
      </div>
    </ContactRegisterStyled>
  );
}

export default ContactRegister;
