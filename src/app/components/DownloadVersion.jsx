import React from "react";
import styled from "styled-components";
import { IMAGE_CONSTANTS } from "../constant/ImageConstants";

const DownLoadVersionStyled = styled.div`
  && {
    &.download {
      width: 100%;
      background-color: #d8edff;
      padding: 30px 0;
      .download-container {
        max-width: 1020px;
        margin: 0 auto;
        display: flex;
        justify-content: space-between;
        align-items: center;
        .notice {
          color: #2699fb;
          font-size: 20px;
          font-weight: bold;
        }
        .block-platform-download {
          .image-platform {
            padding: 0 6px;
          }
        }
      }
    }
    @media (max-width: 768px) {
      &.download {
        .download-container {
          display: block;
          padding: 0 21px;
          .block-platform-download {
            .image-platform {
              padding: 5px 0;
              width: 48%;
              &:nth-child(2) {
                margin-left: 7px;
              }
            }
          }
        }
      }
    }
  }
`;

function DownloadVersion() {
  return (
    <DownLoadVersionStyled className="download">
      <div className="download-container">
        <div className="notice">
          <p>Download latest version</p>
        </div>
        <div className="block-platform-download">
          <img
            className="image-platform"
            src={IMAGE_CONSTANTS.Nintendo}
            alt=""
          />
          <img
            className="image-platform"
            src={IMAGE_CONSTANTS.MicrosoftImage}
            alt=""
          />
          <img className="image-platform" src={IMAGE_CONSTANTS.Steam} alt="" />
        </div>
      </div>
    </DownLoadVersionStyled>
  );
}

export default DownloadVersion;
