import React, { useState } from "react";
import styled from "styled-components";
import { Button } from "@material-ui/core";
import { ReactComponent as Logo } from "../../assets/logo/Gameloft_Logo_F.svg";
import { ReactComponent as LanguageIcon } from "../../assets/icons/languageIcon.svg";

import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Dropdown from "react-bootstrap/Dropdown";
import { socialData, socialDataMobiile } from "../data/DataMumy";

const langaugeData = [
  { id: "en", name: "English" },
  { id: "france", name: "France" },
];

const titleGame = [
  "Gameloft Games",
  "Gameloft Carrers",
  "Gameloft News",
  "Gameloft Forum",
  "Gameloft Corporate",
  "Gameloft Advertising",
  "Gameloft Support",
];

const termOfService = [
  "Terms of Use",
  "Privacy Policy",
  "Cookies Policy",
  "EULA",
  "Legal Notices",
  "Event Rules",
  "Contest Rules",
  "Business Contacts",
];

const FooterStyled = styled.div`
  && {
    &.footer {
      width: 100%;
      height: calc(var(--vh) * 100);
      background-color: #2699fb;
      @media (max-width: 768px) {
        height: 100%;
      }
      .footer {
        &-bottom {
          width: 100%;
          border-top: 1px solid #fff;
          background-color: #2699fb;
          &-container {
            max-width: 1020px;
            margin: 0 auto;
            width: 100%;
            margin-top: 0;
            margin-bottom: 0;
            .info {
              text-align: center;
              color: #fff;
              font-size: 15px;
              padding: 24px 0 29px;
            }
            @media (max-width: 768px) {
              .info {
                font-size: 10px;
                margin: 0 20px;
              }
            }
          }
        }
        &-container {
          max-width: 1020px;
          margin: 0 auto;
          width: 100%;
          color: #fff;
          &-header {
            padding: 143px 0;
            display: flex;
            justify-content: space-between;
            .footer-left {
              .logo {
                svg {
                  width: 414px;
                  height: 129px;
                  filter: invert(1);
                }
              }
              .social {
                .txt {
                  font-size: 25px;
                  font-weight: bold;
                  line-height: 25px;
                }
                .btn-social {
                  .btn {
                    background-color: #fff;
                    border-radius: 0;
                    width: 51px;
                    height: 51px;
                    margin-right: 9px;
                    &.facebook {
                      background: none;
                      padding: 0;
                      min-width: 51px;
                    }
                  }
                }
                .switch-language {
                  margin-top: 32px;
                  .btn-success {
                    color: #2699fb;
                    font-size: 15px;
                    font-weight: 500;
                    background: #fff;
                    border-color: #fff;
                    &:focus {
                      box-shadow: none;
                    }
                    &::after {
                      display: none;
                    }
                  }
                }
              }
            }
            .footer-right {
              color: #fff;
              .others {
                .title {
                  font-size: 25px;
                  font-weight: bold;
                  padding-top: 20px;
                  text-transform: uppercase;
                }
                .title-game {
                  ul {
                    padding: 0;
                    list-style: none;
                    li {
                      font-weight: 400;
                      font-size: 17px;
                    }
                  }
                }
              }
              .terms-service {
                .title {
                  font-size: 25px;
                  font-weight: bold;
                  padding-top: 20px;
                  text-transform: uppercase;
                }
                .link {
                  ul {
                    padding: 0;
                    list-style: none;
                    li {
                      font-weight: 400;
                      font-size: 17px;
                    }
                  }
                }
              }
            }
          }
        }
        @media (max-width: 768px) {
          &-container {
            &-header {
              display: block;
              padding: 0;
              .footer-left {
                .logo {
                  display: flex;
                  justify-content: center;
                  svg {
                    width: 100%;
                    max-width: 274px;
                    filter: invert(1);
                  }
                }
                .social {
                  display: none;
                }
              }
              .footer-right {
                display: flex;
                align-items: center;
                justify-content: space-between;
                margin: 0 20px;
                .others {
                  .title {
                    display: none;
                  }
                  .title-game {
                    ul {
                      li {
                        font-size: 12px;
                        font-weight: bold;
                      }
                    }
                  }
                }
                .terms-service {
                  .title {
                    display: none;
                  }
                  .link {
                    ul {
                      li {
                        font-size: 12px;
                        font-weight: bold;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`;

const FooterSocialMobileStyled = styled.div`
  &.footer-social {
    background-color: #c4e4ff;
    .footer-social {
      &-container {
        width: 100%;
        display: flex;
        flex-direction: column;
        align-items: center;
        .txt {
          font-size: 12px;
          font-weight: bold;
          line-height: 25px;
          color: #2699fb;
          margin: 0;
        }
        .btn-social {
          display: flex;
          align-items: center;
          .social {
            border-radius: 0;
            padding: 10px 5px;
            svg {
              width: 25px;
              height: 25px;
            }
          }
        }
      }
    }
  }
`;

const FooterSocialMobile = () => {
  return (
    <FooterSocialMobileStyled className="footer-social">
      <div className="footer-social-container">
        <p className="txt">Follow Us</p>
        <div className="btn-social">
          {socialDataMobiile.map((item) => {
            return (
              <a
                className={`social ${item.id}`}
                id={item.id}
                href={item.link}
                target="_blank"
                rel="noreferrer"
              >
                <span>{item.name}</span>
              </a>
            );
          })}
        </div>
      </div>
    </FooterSocialMobileStyled>
  );
};

function Footer() {
  const [languageActive, setLanguageActive] = useState("English");
  const isMobileDetect = window.innerWidth < 768;

  return (
    <>
      {isMobileDetect && <FooterSocialMobile />}
      <FooterStyled className="footer">
        <div className="footer-container">
          <div className="footer-container-header">
            <div className="footer-left">
              <div className="logo">
                <Logo />
              </div>
              <div className="social">
                <p className="txt">Follow Us</p>
                <div className="btn-social">
                  {socialData.map((item) => {
                    return (
                      <a href={item.link} target="_blank" rel="noreferrer">
                        <Button className={`btn ${item.id}`} id={item.id}>
                          {item.name}
                        </Button>
                      </a>
                    );
                  })}
                </div>
                <Dropdown className="switch-language">
                  <Dropdown.Toggle variant="success" id="dropdown-basic">
                    <LanguageIcon /> {languageActive}
                    <ExpandMoreIcon />
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    {langaugeData.map((item) => (
                      <Dropdown.Item
                        onClick={() => setLanguageActive(item.name)}
                      >
                        {" "}
                        {item.name}{" "}
                      </Dropdown.Item>
                    ))}
                  </Dropdown.Menu>
                </Dropdown>
              </div>
            </div>
            <div className="footer-right">
              <div className="others">
                <div className="title">
                  <p>Visit</p>
                </div>
                <div className="title-game">
                  <ul>
                    {titleGame.map((item) => (
                      <li>{item}</li>
                    ))}
                  </ul>
                </div>
              </div>
              <div className="terms-service">
                <div className="title">
                  <p>Legal</p>
                </div>
                <div className="link">
                  <ul>
                    {termOfService.map((item) => (
                      <li>{item}</li>
                    ))}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="footer-bottom">
          <div className="footer-bottom-container">
            <p className="info">
              ©2020 Gameloft. All rights reserved. Gameloft and the Gameloft
              logo are trademarks of Gameloft in the U.S. and/or other
              countries. All other trademarks are the property of their
              respective owners.
            </p>
          </div>
        </div>
      </FooterStyled>
    </>
  );
}

export default Footer;
