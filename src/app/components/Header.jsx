import React from "react";
import styled from "styled-components";
import { ReactComponent as Logo } from "../../assets/logo/Gameloft_Logo_F.svg";
import { ReactComponent as Menu } from "../../assets/icons/iconHam.svg";

const HeaderStyled = styled.div`
  &.header {
    width: 100%;
    background: rgba(255, 255, 255, 0.6);
    position: fixed;
    top: 0;
    left: 0;
    z-index: 99;
    .header {
      &-container {
        display: flex;
        align-items: center;
        padding: 19px 80px;
        .logo,
        .menu-hamburger {
          cursor: pointer;
        }
        .logo {
          svg {
            width: calc(100%);
          }
        }
        .menu-hamburger {
          margin-left: auto;
          svg {
            width: calc(100%);
          }
        }
      }
    }
    @media (max-width: 768px) {
      .header {
        &-container {
          padding: 9px 20px;
          .logo {
            svg {
              width: 112px;
            }
          }
          .menu-hamburger {
            margin-left: auto;
            text-align: right;
            svg {
              width: 27px;
            }
          }
        }
      }
    }
  }
`;

function Header() {
  return (
    <HeaderStyled className="header">
      <div className="header-container">
        <div className="logo">
          <Logo />
        </div>
        <div className="menu-hamburger">
          <Menu />
        </div>
      </div>
    </HeaderStyled>
  );
}

export default Header;
