import React from "react";
import styled from "styled-components";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/effect-coverflow";
import { gameCommunityData } from "../data/DataMumy";
import { IMAGE_CONSTANTS } from "../constant/ImageConstants";
import {
  Navigation,
  Pagination,
  FreeMode,
  EffectCoverflow,
  Autoplay,
} from "swiper";
import { Button } from "@material-ui/core";
import ShareIcon from "@material-ui/icons/Share";

const SpecialEventStyled = styled.div`
  && {
    &.special {
      width: 100%;
      height: calc(var(--vh) * 100 - 85);
      background-color: #d5ecff;
      .swiper-pagination {
        display: flex;
        align-items: center;
        justify-content: center;
        margin: 50px 0;
        .swiper-pagination-bullet {
          width: 40px;
          height: 40px;
          text-align: center;
          display: flex;
          border-radius: 100%;
          align-items: center;
          justify-content: center;
          color: #2699fb;
          cursor: pointer;
        }
      }
      .special {
        &-container {
          max-width: 1020px;
          margin: 0 auto;
          .header {
            padding-top: 81px;
            color: #2699fb;
            .title {
              font-weight: bold;
              font-size: 45px;
              padding-bottom: 20px;
            }
            .subTitle {
              font-size: 25px;
              color: #2699fb;
              font-weight: 400;
            }
          }
        }
      }
      .slider-block {
        margin-top: 70px;
        .slider-special {
          &.swiper-3d {
            perspective: 1920px;
          }
          .swiper-slide {
            .slider-container {
              display: flex;
              align-items: center;
              justify-content: center;
              position: relative;
              .card-info-deActive {
                transition: all 0.3s;
                position: absolute;
                max-width: 437px;
                width: 437px;
                top: 0;
                padding: 34px 41px;
                color: #fff;
                height: 100%;
                .title {
                  height: 100%;
                  display: flex;
                  flex-direction: column;
                  justify-content: flex-end;
                  font-size: 35px;
                  font-weight: bold;
                  span {
                    display: block;
                  }
                }
              }
            }
            .card-info-active {
              position: absolute;
              transition: all 0.3s;
              max-width: 437px;
              top: 0;
              padding: 34px 41px;
              color: #fff;
              display: flex;
              flex-direction: column;
              height: 100%;
              .header {
                display: flex;
                align-items: center;
                .title {
                  font-size: 35px;
                  font-weight: bold;
                }
                .share {
                  margin-left: auto;
                  transition: all 0.3s;
                  position: relative;
                  z-index: 2;
                  color: #000;
                  background: #fff;
                  width: 40px;
                  height: 40px;
                  border-radius: 100%;
                  svg {
                    position: absolute;
                    left: 6px;
                    bottom: 9px;
                  }
                  &:hover {
                    color: #2699fb;
                  }
                }
              }
              .here {
                display: block;
                padding-bottom: 5px;
                border-bottom: 1px solid #fff;
                width: fit-content;
                font-size: 35px;
                font-weight: bold;
              }
              .subTitle {
                padding-top: 15px;
                display: -webkit-box;
                max-height: 6rem;
                -webkit-box-orient: vertical;
                overflow: hidden;
                text-overflow: ellipsis;
                white-space: normal;
                -webkit-line-clamp: 3;
                line-height: 1.6rem;
                word-break: break-word;
              }
            }
            .card-info-bottom {
              margin-top: auto;
              display: flex;
              justify-content: center;
              button {
                color: #fff;
                border: 1px solid;
                padding: 15px 106px;
                text-align: center;
                border-radius: 0;
                text-transform: lowercase;
                font-size: 21px;
                font-weight: bold;
                &:hover {
                  background-color: #2699fb;
                  border-color: #2699fb;
                }
              }
            }
          }
        }
      }
    }
    @media (max-width: 768px) {
      &.special {
        .swiper-pagination {
          display: none;
        }
        .special {
          &-container {
            padding: 0 21px;
            .header {
              padding-top: 32px;
              .title {
                font-size: 25px;
              }
              .subTitle {
                font-size: 20px;
              }
            }
          }
        }
      }
      .slider-block {
        padding-bottom: 100px;
        .slider-special {
          .swiper-slide {
            .slider-container {
              img {
                max-width: 350px;
              }
            }
            .card-info-active {
              button {
                padding: 15px 68px !important;
              }
            }
          }
        }
      }
    }
  }
`;

function SpecialEvent() {
  const isMobileDetect = window.innerWidth < 768;

  return (
    <SpecialEventStyled className="special">
      <div className="special-container">
        <div className="header">
          <div className="title">Special Events & Promotional</div>
          <div className="subTitle">
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
            nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
            erat, sed diam voluptua. At vero eos.
          </div>
        </div>
      </div>
      <div className="slider-block">
        <Swiper
          effect={"coverflow"}
          grabCursor={true}
          centeredSlides={true}
          loop={true}
          autoplay={{
            delay: 3000,
            disableOnInteraction: true,
          }}
          coverflowEffect={{
            rotate: 0,
            stretch: 0,
            depth: 100,
            modifier: isMobileDetect ? 0 : 3,
            slideShadows: 0,
          }}
          slidesPerView={isMobileDetect ? 1 : 3}
          pagination={{
            el: ".swiper-pagination",
            clickable: true,
            renderBullet: function (i, className) {
              return `
           <button class="${className}">
          <svg class="progress" width="41" height="41"><circle class="circle-origin" r="20" cx="20.5" cy="20.5"></circle></svg><span>${
            i + 1
          }</span>
        </button>
              `;
            },
          }}
          navigation={{
            nextEl: ".slide-next-special",
            prevEl: ".slide-prev-special ",
          }}
          modules={[
            EffectCoverflow,
            Navigation,
            Pagination,
            FreeMode,
            Autoplay,
          ]}
          className="slider-special"
        >
          {gameCommunityData.map((item, index) => (
            <SwiperSlide>
              {({ isActive }) => (
                <div className="slider-container">
                  <img
                    src={IMAGE_CONSTANTS.SpecialCard}
                    alt="img"
                    className={`thumbnail slide-${item.id}`}
                  />
                  <div
                    style={!isActive ? { opacity: 1 } : { opacity: 0 }}
                    className="card-info-deActive"
                  >
                    <div className="title">
                      short title <span>here</span>{" "}
                    </div>
                  </div>
                  <div
                    style={isActive ? { opacity: 1 } : { opacity: 0 }}
                    className="card-info-active"
                  >
                    <div className="card-info-top">
                      <div className="header">
                        <div className="title">short title</div>
                        <div className="share">
                          <ShareIcon className="icon hoverIconAroud" />
                        </div>
                      </div>
                      <span className="here">here</span>
                      <div className="subTitle">
                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                        sed diam nonumy eirmod tempor invidunt ut labore et
                        dolore magna aliquyam erat, sed diam voluptua. At vero
                        eos.
                      </div>
                    </div>
                    <div className="card-info-bottom">
                      <Button className="readmore-btn">read more</Button>
                    </div>
                  </div>
                </div>
              )}
            </SwiperSlide>
          ))}
          <div class="swiper-pagination"></div>
        </Swiper>
      </div>
    </SpecialEventStyled>
  );
}

export default SpecialEvent;
