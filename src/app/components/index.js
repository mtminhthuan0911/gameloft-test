import Banner from "./Banner";
import ContactRegister from "./ContactRegister";
import DownloadVersion from "./DownloadVersion";
import Header from "./Header";
import Footer from "./Footer";
import Community from "./Community";
import ExclusiteContent from "./ExclusiteContent";
import SpecialEvent from "./SpecialEvent";

export {
  Banner,
  ContactRegister,
  DownloadVersion,
  Header,
  Footer,
  Community,
  ExclusiteContent,
  SpecialEvent,
};
