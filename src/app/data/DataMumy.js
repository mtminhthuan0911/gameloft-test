import { IMAGE_CONSTANTS, ICONS_CONSTANTS } from "../constant/ImageConstants";

const {
  LinkedMobile,
  YoutubeMobile,
  TwitterMobile,
  FacebookMobile,
  FaceBook,
  In,
  Twitter,
  Youtube,
  Instagram,
  FacebookCommunity,
  TwitterCommunity,
} = ICONS_CONSTANTS;

export const gameCommunityData = [
  {
    id: 0,
    img: IMAGE_CONSTANTS.CommmunityImage,
    shortTitle: "March of ",
    description:
      "Conntent with facebook March of Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor magnaaliquyam erat, sed diam voluptua. At vero eos",
    category: "facebook",
  },
  {
    id: 1,
    img: IMAGE_CONSTANTS.CommmunityImage_1,
    shortTitle: "Asphalt 9",
    description:
      "Conntent with facebook Asphalt 9 1 Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor magnaaliquyam erat, sed diam voluptua. At vero eos",
    category: "facebook",
  },
  {
    id: 2,
    img: IMAGE_CONSTANTS.CommmunityImage_2,
    shortTitle: "Disney Magic",
    description:
      "Conntent with facebook Disney Magic 2 Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor magnaaliquyam erat, sed diam voluptua. At vero eos",
    category: "facebook",
  },
  {
    id: 3,
    img: IMAGE_CONSTANTS.CommmunityImage_3,
    shortTitle: "War Planet ",
    description:
      "Conntent with instagram War Planet Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor magnaaliquyam erat, sed diam voluptua. At vero eos",
    category: "instagram",
  },
  {
    id: 4,
    img: IMAGE_CONSTANTS.CommmunityImage_4,
    shortTitle: "Dragon Mania.",
    description:
      "Conntent with instagram 1 Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor magnaaliquyam erat, sed diam voluptua. At vero eos",
    category: "instagram",
  },
  {
    id: 5,
    img: IMAGE_CONSTANTS.CommmunityImage_5,
    shortTitle: "Sniper Fury",
    description:
      "Conntent with instagram 1 Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor magnaaliquyam erat, sed diam voluptua. At vero eos",
    category: "instagram",
  },
  {
    id: 6,
    img: IMAGE_CONSTANTS.CommmunityImage_1,
    shortTitle: "Asphalt 8",
    description:
      "Conntent with twitter 1 Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor magnaaliquyam erat, sed diam voluptua. At vero eos",
    category: "twitter",
  },
  {
    id: 7,
    img: IMAGE_CONSTANTS.CommmunityImage_3,
    shortTitle: "Modern Combat",
    description:
      "Conntent with twitter 1 March of Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor magnaaliquyam erat, sed diam voluptua. At vero eos",
    category: "twitter",
  },
  {
    id: 8,
    img: IMAGE_CONSTANTS.CommmunityImage_4,
    shortTitle: "Blitz Brigade",
    description:
      "Conntent with twitter 1 March of Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor magnaaliquyam erat, sed diam voluptua. At vero eos",
    category: "twitter",
  },
  {
    id: 9,
    img: IMAGE_CONSTANTS.CommmunityImage_1,
    shortTitle: "Asphalt 8",
    description:
      "Conntent with twitter 1 March of Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor magnaaliquyam erat, sed diam voluptua. At vero eos",
    category: "twitter",
  },
];

export const ExclusiveData = [
  {
    id: 0,
    img: IMAGE_CONSTANTS.CommmunityImage,
    shortTitle: "March of ",
    description:
      "Conntent with facebook 1 Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor magnaaliquyam erat, sed diam voluptua. At vero eos",
    category: "facebook",
  },
  {
    id: 1,
    img: IMAGE_CONSTANTS.CommmunityImage_1,
    shortTitle: "Asphalt 9",
    description:
      "conntent with facebook 1 Conntent with facebook March of Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor magnaaliquyam erat, sed diam voluptua. At vero eos",
    category: "facebook",
  },
  {
    id: 2,
    img: IMAGE_CONSTANTS.CommmunityImage_2,
    shortTitle: "Disney Magic",
    description:
      "conntent with facebook 1 Conntent with facebook March of Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor magnaaliquyam erat, sed diam voluptua. At vero eos",
    category: "facebook",
  },
  {
    id: 3,
    img: IMAGE_CONSTANTS.CommmunityImage_3,
    shortTitle: "War Planet ",
    description:
      "conntent with facebook 1 Conntent with facebook March of Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor magnaaliquyam erat, sed diam voluptua. At vero eos",
    category: "facebook",
  },
  {
    id: 4,
    img: IMAGE_CONSTANTS.CommmunityImage_4,
    shortTitle: "Dragon Mania.",
    description:
      "conntent with facebook 1 Conntent with facebook March of Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor magnaaliquyam erat, sed diam voluptua. At vero eos",
    category: "facebook",
  },
  {
    id: 5,
    img: IMAGE_CONSTANTS.CommmunityImage_5,
    shortTitle: "Sniper Fury",
    description:
      "conntent with facebook 1 Conntent with facebook March of Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor magnaaliquyam erat, sed diam voluptua. At vero eos",
    category: "facebook",
  },
  {
    id: 6,
    img: IMAGE_CONSTANTS.CommmunityImage_1,
    shortTitle: "Asphalt 8",
    description:
      "conntent with facebook 1 Conntent with facebook March of Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor magnaaliquyam erat, sed diam voluptua. At vero eos",
    category: "facebook",
  },
  {
    id: 7,
    img: IMAGE_CONSTANTS.CommmunityImage_3,
    shortTitle: "Modern Combat",
    description:
      "conntent with facebook 1 Conntent with facebook March of Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor magnaaliquyam erat, sed diam voluptua. At vero eos",
    category: "facebook",
  },
  {
    id: 8,
    img: IMAGE_CONSTANTS.CommmunityImage_4,
    shortTitle: "Blitz Brigade",
    description:
      "conntent with facebook 1 Conntent with facebook March of Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor magnaaliquyam erat, sed diam voluptua. At vero eos",
    category: "facebook",
  },
  {
    id: 9,
    img: IMAGE_CONSTANTS.CommmunityImage_1,
    shortTitle: "Asphalt 8",
    description: "conntent with facebook 1",
    category: "facebook",
  },
];

export const socialData = [
  {
    id: "facebook",
    name: <FaceBook />,
    link: "https://www.facebook.com/GameloftViet/?brand_redir=216238295505",
  },
  { id: "in", name: <In />, link: "https://www.instagram.com/gameloft/" },
  {
    id: "twitter",
    name: <Twitter />,
    link: "https://www.linkedin.com/company/gameloft/",
  },
  {
    id: "youtube",
    name: <Youtube />,
    link: "https://www.youtube.com/user/gameloft",
  },
];

export const socialDataCommunity = [
  {
    id: "twitter",
    name: <TwitterCommunity />,
    link: "https://www.facebook.com/GameloftViet/?brand_redir=216238295505",
  },
  {
    id: "instagram",
    name: <Instagram />,
    link: "https://www.instagram.com/gameloft/",
  },
  {
    id: "facebook",
    name: <FacebookCommunity />,
    link: "https://twitter.com/gameloft",
  },
];

export const socialDataMobiile = [
  {
    id: "facebook",
    name: <FacebookMobile />,
    link: "https://www.facebook.com/GameloftViet/?brand_redir=216238295505",
  },
  {
    id: "in",
    name: <LinkedMobile />,
    link: "https://www.instagram.com/gameloft/",
  },
  {
    id: "twitter",
    name: <TwitterMobile />,
    link: "https://www.linkedin.com/company/gameloft/",
  },
  {
    id: "youtube",
    name: <YoutubeMobile />,
    link: "https://www.youtube.com/user/gameloft",
  },
];
